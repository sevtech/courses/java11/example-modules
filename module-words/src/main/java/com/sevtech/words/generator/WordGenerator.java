package com.sevtech.words.generator;

import com.sevtech.words.Word;

/**
 * Esta clase es solo accesible por el módulo Words y Dictionary
 */
public class WordGenerator {

    private int count = 0;

    public Word next() {
        count++;
        return new Word("word " + count);
    }

}
