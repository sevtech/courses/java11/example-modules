package com.sevtech.logging;

public interface Logger {

    void error(String msg);

    void info(String msg);

    void debug(String msg);

}
