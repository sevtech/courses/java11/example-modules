# example-modules
Proyecto de ejemplo con 5 modulos para mostrar el uso de *ModuleSystem*.

Se compila mediante:
~~~~
mvn clean install
~~~~
Se necesita la JDK 11 para compilarlo y arrancarlo.
Para arrancarlo existen los scripts *run.cmd* y *run.sh* (Indicar en ambos scripts la ruta donde tengas la JDK) .
