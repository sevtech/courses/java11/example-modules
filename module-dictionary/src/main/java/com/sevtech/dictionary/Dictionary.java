package com.sevtech.dictionary;

import com.sevtech.words.Word;
import com.sevtech.words.generator.WordGenerator;

public class Dictionary {

    private final WordGenerator generator = new WordGenerator();

    public Word getWord() {
        return generator.next();
    }

}
