module com.sevtech.logging.console {

    requires com.sevtech.logging;

    provides com.sevtech.logging.Logger with com.sevtech.logging.console.ConsoleLogger;

}
