module com.sevtech.app {

    requires com.sevtech.dictionary;
    requires com.sevtech.logging;

    uses com.sevtech.logging.Logger;

}
